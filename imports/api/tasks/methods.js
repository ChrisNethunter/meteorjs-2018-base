// Methods related to Collection
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Tasks } from './tasks.js';

Meteor.methods({
  	'tasks.insert'(task) {
	    
	    // Make sure the user is logged in before inserting a task
	    if (! Meteor.userId()) {
	      throw new Meteor.Error('not-authorized');
	    }

	    Tasks.insert({
				      name 		  : task.name ,
				      description : task.description,
				      createdAt   : new Date(),
				      owner       : Meteor.userId()
				    });
  	},

  	'tasks.edit'(task) {
	    
	    // Make sure the user is logged in before inserting a task
	    if (! Meteor.userId()) {
	      throw new Meteor.Error('not-authorized');
	    }

	    Tasks.update({_id: task._id}, { $set : { name : task.name  , description : task.description , modifyAt : new Date()}  } );
  	},

  	'tasks.remove'(_id) {
	    
	    // Make sure the user is logged in before inserting a task
	    if (! Meteor.userId()) {
	      throw new Meteor.Error('not-authorized');
	    }

	    Tasks.remove({_id:_id});
  	},



});