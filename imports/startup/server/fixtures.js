// Import server startup through a single index entry point

import './fixtures.js';
import './register-api.js';

import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {
  // code to run on server at startup
});
